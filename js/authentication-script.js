//         const authenticationData = {
//             Username : 'vladimir.khazin@icssolutions.ca',
//             Password : 'qWerty1@3456'
//         };

//         return new Promise(function (resolve, reject) {
  //           const authenticationDetails = new AmazonCognitoIdentity.AuthenticationDetails(authenticationData);
  //           const userData = {
  //               Username : authenticationData.Username,
  //               Pool : userPool
  //           };
  //           const cognitoUser = new AmazonCognitoIdentity.CognitoUser(userData);

  //           cognitoUser.authenticateUser(authenticationDetails, {
  //               onSuccess: function (session) {
  // //                 console.log('access token: ' + result.getAccessToken().getJwtToken());
  //                 /*
  //                 Use the idToken for Logins Map when Federating User Pools with identity pools 
  //                 or when passing through an Authorization Header to an API Gateway Authorizer
  //                 */
  // //                 console.log('idToken: ' + result.idToken.jwtToken);
  //                 return resolve({
  //                   user: cognitoUser,
  //                   session: session
  //                 });

  //               },
  //               onFailure: function(err) {
  //                 return reject(err);
  //               },

  //           });
  //         });

        const validateOAuth = (code) => {
          const validationUrl = domainUrl + '/oauth2/token';
          const params = 'redirect_uri=' + encodeURIComponent(getCurrentUrl()) + 
            '&grant_type=authorization_code'+
            '&scope=openid+email' +
            '&client_id=' + clientId +
            '&code=' + encodeURIComponent(code);

          fetch(validationUrl, {
              method : "POST",
              body: params,
              headers: {
                'Content-type': 'application/x-www-form-urlencoded'
              }
            })
            .then(response => {
              return new Promise((resolve, reject) => {
                if (response.ok == true) {
                  return resolve(response.json());
                } else {
                  return resolve(redirectToLogin());
                }
              });
            })
            .then(responseJson => {
              console.log(responseJson);
              return responseJson;
            });
        };

//         isUserLoggedIn()
//           .then(loginResponse => {
//             return new Promise((resolve, reject) => {
//               if (loginResponse.code && loginResponse.code == 401) {
//                 console.log('User is not authenticated.');
//                 return resolve(authenticateUser());
//               } else {
//                 console.log('User is already singed-in');
//                 return resolve(userSessionInfo);
//               }
//             });
//           })
//           .then(userSessionInfo => {
//             console.log('Fetching user attributes');
//             return getUserAttributes(userSessionInfo.user)   
//           })
//           .then(userData => {
//             console.log(JSON.stringify(userData))
//           })